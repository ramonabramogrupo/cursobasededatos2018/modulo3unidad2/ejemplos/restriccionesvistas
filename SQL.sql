﻿CREATE DATABASE restriccionesVistas;

USE restriccionesVistas;

CREATE TABLE clientes(
  id int AUTO_INCREMENT,
  edad int,
  PRIMARY KEY (id)
  );

ALTER TABLE clientes ADD COLUMN edad1 int;

CREATE OR REPLACE VIEW vista1 AS 
  SELECT * FROM clientes c
  WHERE c.edad>=0 AND c.edad1>c.edad
  WITH LOCAL CHECK OPTION;

TRUNCATE clientes;

INSERT IGNORE INTO vista1 VALUES
  (DEFAULT, -10,10), -- la edad no cumple
  (DEFAULT,10,0), -- la edad1 es menor que edad
  (DEFAULT, 10, 20);  -- correcto

SELECT * FROM vista1;
SELECT * FROM clientes c;

CREATE TABLE ventas(
  id int AUTO_INCREMENT,
  cliente int,
  fecha date,
  cantidad int,
  PRIMARY KEY(id));

ALTER TABLE ventas 
  ADD CONSTRAINT FKVentasClientes FOREIGN KEY (cliente)
  REFERENCES clientes(id) ON DELETE CASCADE ON UPDATE CASCADE;

-- vista2 (sobre la tabla ventas)
-- fecha de ventas menor que hoy
-- cantidad mayor que 10 y menor que 100

 CREATE OR REPLACE VIEW vista2 AS 
  SELECT * FROM ventas
  WHERE fecha<NOW() AND cantidad BETWEEN 10 AND 100
  WITH LOCAL CHECK OPTION;

TRUNCATE ventas;

INSERT IGNORE INTO vista2 VALUES
  (DEFAULT,NULL,'2020/1/1',0),
  (DEFAULT,NULL,'2020/1/1',80),
  (DEFAULT,NULL,'2018/1/1',80);

SELECT * FROM ventas v;

-- vista3 
-- cantidad este entre 10 y 100
  
  CREATE OR REPLACE VIEW vista3 AS
    SELECT * FROM ventas 
    WHERE cantidad BETWEEN 10 AND 100
    WITH LOCAL CHECK OPTION;

-- vista4
-- con las dos restricciones
CREATE OR REPLACE VIEW vista4 AS
  SELECT * FROM vista3 
    WHERE fecha<NOW()
    WITH LOCAL CHECK OPTION;

TRUNCATE ventas;

INSERT IGNORE INTO vista3 VALUES
  (DEFAULT,NULL,'2020/1/1',0), -- no
  (DEFAULT,NULL,'2020/1/1',80), -- si
  (DEFAULT,NULL,'2018/1/1',80); -- si

SELECT * FROM ventas;

TRUNCATE ventas;

INSERT IGNORE INTO vista4 VALUES
  (DEFAULT,NULL,'2020/1/1',0), -- no
  (DEFAULT,NULL,'2020/1/1',80), -- no
  (DEFAULT,NULL,'2018/1/1',80), -- si
  (DEFAULT,NULL,'2018/1/1',0); -- si

SELECT * FROM ventas;

CREATE OR REPLACE VIEW vista5 AS
  SELECT * FROM vista3 
    WHERE fecha<NOW()
    WITH CASCADED CHECK OPTION;

TRUNCATE ventas;

INSERT IGNORE INTO vista5 VALUES
  (DEFAULT,NULL,'2020/1/1',0), -- no
  (DEFAULT,NULL,'2020/1/1',80), -- no
  (DEFAULT,NULL,'2018/1/1',80), -- si
  (DEFAULT,NULL,'2018/1/1',0); -- no

SELECT * FROM ventas v;

-- crear un campo nuevo llamado ventas
ALTER TABLE clientes ADD COLUMN ventas int;

TRUNCATE ventas;
DELETE from clientes;

INSERT INTO clientes VALUES 
  (2,40,45,0);

INSERT IGNORE INTO vista5 VALUES
  (DEFAULT,2,'2018/1/1',40), 
  (DEFAULT,2,'2018/1/1',80); 
  

-- con update me diga cuentas ventas tiene ese cliente
UPDATE 
  clientes c JOIN 
  (SELECT 
    cliente,COUNT(*) total 
    FROM ventas GROUP BY cliente) c1
  ON c.id=c1.cliente
  set c.ventas=c1.total;


  










 

